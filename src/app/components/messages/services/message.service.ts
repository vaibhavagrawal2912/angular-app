import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: string[] = new Array();

  add(message: string): void {
    this.messages.push(message);
  }
  clear(): void {
    this.messages.length = 0;
  }
  getMessages(): string[] {
    return this.messages;
  }
  constructor() { }
}
