
export class JaxUrls {
    private static serverIdentity = `http://localhost:3000`;

    public static readonly getAllHeroes = `${JaxUrls.serverIdentity}/heroes`;

    public static readonly getHero = `${JaxUrls.serverIdentity}/hero`;

    public static readonly updateHero = `${JaxUrls.serverIdentity}/updateHero`;

    public static readonly  addHero = `${JaxUrls.serverIdentity}/addHero`

    public static readonly deleteHero = `${JaxUrls.serverIdentity}/deleteHero`

    public static readonly searchHero = `${JaxUrls.serverIdentity}/searchHeroes`

}