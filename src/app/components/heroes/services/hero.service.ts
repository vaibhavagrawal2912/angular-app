import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Hero } from 'src/app/dataModals/hero';
import { MessageService } from 'src/app/components/messages/services/message.service';
import { JaxUrls } from 'src/app/components/heroes/constants/jaxURLS';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(JaxUrls.getAllHeroes)
      .pipe(
        tap(heroes => this.log('fetched Heroes')),
        catchError(this.handleError('getHeroes', [])));
  }

  getHero(id: number): Observable<Hero> {
    return this.http.get<Hero>(`${JaxUrls.getHero}/${id}`).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<Hero>(`getHero id=${id}`)));
  }

  updateHero(hero: Hero): Observable<any> {
    return this.http.put(`${JaxUrls.updateHero}`, hero, this.httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${hero.id}`)),
      catchError(this.handleError<any>('updateHero')));
  }

  addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(JaxUrls.addHero, hero, this.httpOptions).pipe(
      tap((hero: Hero) => this.log(`added hero w/ id=${hero.id}`)),
      catchError(this.handleError<Hero>('addHero')));
  }

  deleteHero (hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${JaxUrls.deleteHero}/${id}`;

    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(id => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero')));
  }

  searchHeroes(searchTerm: string): Observable<Hero[]> {
    if (!searchTerm.trim()) {
      return of([]);
    }
    let url = `${JaxUrls.searchHero}?searchTerm=${searchTerm}`
    return this.http.get<Hero[]>(url, this.httpOptions).pipe(
      tap(term => this.log(`found heroes matching "${searchTerm}"`)),
      catchError(this.handleError<Hero[]>('searchHeroes', [])));
  }

  private log(message: string) {
    this.messageService.add(`Message: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    }
  }
}
